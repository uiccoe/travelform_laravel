<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Views / Endpoints
    |--------------------------------------------------------------------------
    |
    | Set your login page, or login routes, here. If you provide a view,
    | that will be rendered. Otherwise, it will redirect to a route.
    |
     */

    'idp_login'     => '/Shibboleth.sso/Login',
    'idp_logout'    => '/Shibboleth.sso/Logout',
    'authenticated' => '/',
    'protected' => [ // Array of routes that should be protected with Shibboleth, invoked by custom middleware
        'home'
    ],

    /*
    |--------------------------------------------------------------------------
    | Emulate an IdP
    |--------------------------------------------------------------------------
    |
    | In case you do not have access to your Shibboleth environment on
    | homestead or your own Vagrant box, you can emulate a Shibboleth
    | environment with the help of Shibalike.
    |
    | The password is the same as the username.
    |
    | Do not use this in production for literally any reason.
    |
     */

    'emulate_idp'       => env('SHIBALIKE', false),
    'emulate_idp_users' => array(
        'admin' => array(
            'uid'         => 'admin',
            'displayName' => 'Admin User',
            'givenName'   => 'Admin',
            'sn'          => 'User',
            'mail'        => 'admin@uic.edu',
            'eppn'        => 'admin@uic.edu',
            'iTrustUIN'   => '11111',
            'ou'          => 'admin-org',
            'primary-affiliation' => 'admin-primary-affiliation',
            'affiliation' => 'admin-affiliation'
        ),
        'staff' => array(
            'uid'         => 'staff',
            'displayName' => 'Staff User',
            'givenName'   => 'Staff',
            'sn'          => 'User',
            'mail'        => 'staff@uic.edu',
            'eppn'        => 'staff@uic.edu',
            'iTrustUIN'   => '22222',
            'ou'          => 'staff-org',
            'primary-affiliation' => 'staff-primary-affiliation',
            'affiliation' => 'staff-affiliation'
        ),
        'user'  => array(
            'uid'         => 'user',
            'displayName' => 'Test User',
            'givenName'   => 'Test',
            'sn'          => 'User',
            'mail'        => 'user@uic.edu',
            'eppn'        => 'user@uic.edu',
            'iTrustUIN'   => '33333',
            'ou'          => 'user-org',
            'primary-affiliation' => 'user-primary-affiliation',
            'affiliation' => 'user-affiliation',
        ),
    ),

    /*
    |--------------------------------------------------------------------------
    | Server Variable Mapping
    |--------------------------------------------------------------------------
    |
    | Change these to the proper values for your IdP.
    |
     */

    'entitlement' => 'entitlement',

    'user' => [
        // fillable user model attribute => server variable
        'email'       => 'mail',
        'name'        => 'displayName',
        'first_name'  => 'givenName',
        'last_name'   => 'sn',
        'netid'       => 'uid',
        'uin'         => 'iTrustUIN',
        'org'         => 'ou',
        'primary_affiliation' => 'primary-affiliation',
        'affiliation' => 'affiliation',
    ],

    /*
    |--------------------------------------------------------------------------
    | User Creation and Groups Settings
    |--------------------------------------------------------------------------
    |
    | Allows you to change if / how new users are added
    |
     */

    'add_new_users' => env('SHIBBOLETH_CREATE_USER', false), // Should new users be added automatically if they do not exist?

    /*
    |--------------------------------------------------------------------------
    | JWT Auth
    |--------------------------------------------------------------------------
    |
    | JWTs are for the front end to know it's logged in
    |
    | https://github.com/tymondesigns/jwt-auth
    | https://github.com/StudentSystemServices/Laravel-Shibboleth-Service-Provider/issues/24
    |
     */

    'jwtauth' => env('JWTAUTH', false),
);
