<?php

return [
    'dept' => [
        'CS' => 'Computer Science',
        'ECE' => 'Electrical and Computer Engineering',
        'MIE' => 'Mechanical and Industrial Engineering',
        'CIVIL' => 'Civil and Materials Engineering',
        'CHEME' => 'Chemical Engineering',
        'BIOE' => 'Bioengineering',
        'ADMIN'=> 'Administration',
    ]
];
