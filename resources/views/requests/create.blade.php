@extends('layout')

@section('scripts')
    {{HTML::script('js/requests/create.js')}}
@endsection

@section('title', 'Create a New Request')

@section('New Request', 'active')

@section('content')
    @include('alerts.success')
    <form id="request" action="/request/submit" enctype="multipart/form-data" method="post">
    @csrf

    <!-- Destination and Travel Date -->

        <div class="row">
            <div class="col-md-6 mb-3">
                <label for="destination">Destination(s)<sup><span
                            style="color: red">*</span></sup></label>
                <input type="text" class="form-control" id="destination" name="destination"
                       placeholder="" required>
                <div class="invalid-feedback">
                    Please enter your destination(s).
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <label for="departure_date">Date of Departure<sup><span
                            style="color: red">*</span></sup></label>
                <input id="datepicker" class="form-control" onchange="checkDate()" name="departure_date"
                       width="100%" class="fa" required/>
                <script>
                    $('#datepicker').datepicker({
                        uiLibrary: 'bootstrap4',
                        icons: {
                            rightIcon: '<i class="material-icons">date_range</i>'
                        }
                    });
                </script>
                <p><span id="error_message" style="color: red; font-size: small;"></span></p>
            </div>
        </div>


        <!-- Duration and Requested amount -->

        <div class="row">
            <div class="col-md-6 mb-3">
                <label for="duration">Duration of Trip (days)<sup><span
                            style="color: red">*</span></sup></label>
                <input type="number" class="form-control" pattern=" 0+\.[0-9]*[1-9][0-9]*$"
                       onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="duration"
                       id="duration" required>
                <div class="invalid-feedback">
                    Please enter a duration
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <label for="expense_amount">Estimated Amount of Request</label>
                <input type="text" class="form-control" name="expense_amount" id="expense_amount"
                       onkeyup="return validateAmount()">
                <div class="invalid-feedback">
                    Please enter your estimated amount.
                </div>
            </div>
        </div>


        <!-- Source of funding -->

        <div class="mb-3">
            <label for="funding_source">Source of Funding</label>
            <input type="text" class="form-control" name="funding_source" id="funding_source"
                   placeholder="">
            <div class="invalid-feedback">
                Please enter funding source.
            </div>
        </div>


        <!-- Purpose of Trip -->

        <div class="mb-3">
            <label for="purpose">Purpose of Trip (i.e name(s) of event(s), etc.)<sup><span
                        style="color: red">*</span></sup></label>
            <textarea type="text" rows="3" class="form-control" name="purpose" id="purpose"
                      placeholder="" required></textarea>
            <div class="invalid-feedback">
                Please enter the purpose of your trip.
            </div>
        </div>


        <!-- Coverage of Classes -->

        <div class="mb-3">
            <label for="coverage">Coverage of Classes and/or Other Duties (Please indicate faculty
                member acting on your behalf as Head during your absence)</label>
            <textarea type="text" rows="3" class="form-control" name="coverage" id="coverage"
                      placeholder=""></textarea>
            <div class="invalid-feedback">
                Please enter how you plan to cover your duties.
            </div>
        </div>

        <p><span style="color: red">*</span> indicates required field.</p>

        <button class="mt-2 align-content-center btn btn-primary btn-md" onclick="confirmInput();"
                type="button" data-toggle="modal" data-target="#confirm-submit">Submit Request
        </button>


        <div class="modal fade bd-example-modal-sm" id="confirm-submit" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to
                            submit?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p><b>Destination</b>: <span id="display_destination"></span></p>
                        <p><b>Date of Departure</b>: <span id="display_departureDate"></span></p>
                        <p><b>Duration of Trip(days)</b>: <span id="display_duration"></span></p>
                        <p><b>Amount of Request</b>: <span id="display_amount"></span></p>
                        <p><b>Source of Funding</b>: <span id="display_funding"></span></p>
                        <p><b>Purpose of Trip</b>: <span id="display_purpose"></span></p>
                        <p><b>Coverage of Classes and/or other Duties</b>: <span
                                id="display_coverage"></span></p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel
                        </button>
                        <button type="submit" class="btn btn-primary">Yes, submit it!</button>
                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection
