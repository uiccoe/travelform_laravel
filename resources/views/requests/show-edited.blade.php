@extends('layout')

@section('content')
    <div style="margin-top: -12%;" class="mb-5 print-button">
        <p>
            <button style="float: right" class="align-content-center btn btn-primary btn-md"
                    onClick="window.print()"><i class="fa fa-print"></i> Print
            </button>
        </p>
    </div>
    <br>
    <p style="float: left"><span
            class="text-dark"><strong>Date of Request</strong></span>: {{ date("F j, Y", strtotime($request->request_date)) }}
    </p>
    <p style="float: right"><span
            class="text-dark"><strong>Department</strong></span>: {{ Config::get("constants.dept.{$request->user->dept}") }}
    </p>

    <table class="table table-bordered table-condensed">
        <tbody>
        <tr>
            <td>
                <h6>
                    <strong>Full Name</strong>
                </h6>
                <span>{{ $request->user->name }}</span>
            </td>
        </tr>
        <tr>
            <td>
                <h6>
                    <strong>Destination</strong>
                </h6>
                @if($request->destination != $history->destination)
                    <p style="color: #bd564f"><em><small>Before: {{ $history->destination }}</small></em></p>
                @endif
                <span>{{ $request->destination }}</span>
            </td>
        </tr>
        <tr>
            <td>
                <h6>
                    <strong>Date of Departure</strong>
                </h6>
                @if($request->departure_date != $history->departure_date)
                    <p style="color: #bd564f"><em><small>Before: {{ date("F j, Y", strtotime($history->departure_date)) }}</small></em></p>
                @endif
                <span>{{ date("F j, Y", strtotime($request->departure_date)) }}</span>
            </td>
        </tr>
        <tr>
            <td>
                <h6>
                    <strong>Duration of Trip (days)</strong>
                </h6>
                @if($request->duration != $history->duration)
                    @if($request->duration > 1)
                        <p style="color: #bd564f"><em><small>Before: {{ $history->duration }} days</small></em></p>
                    @elseif($request->duration == 1)
                        <p style="color: #bd564f"><em><small>Before: {{ $history->duration }} day</small></em></p>
                    @endif
                @endif
                @if($request->duration > 1)
                    <span>{{ $request->duration }} days</span>
                @elseif($request->duration == 1)
                    <span>{{ $request->duration }} day</span>
                @endif
            </td>
        </tr>
        <tr>
            <td>
                <h6>
                    <strong>Estimated Amount of Request</strong>
                </h6>
                @if($request->expense_amount != $history->expense_amount)
                    <p style="color: #bd564f"><em><small>Before: {{ $history->expense_amount }}</small></em></p>
                @endif
                @if (! is_null($request->expense_amount))
                    <span>$</span>
                @endif
                <span>{{ $request->expense_amount }}</span>
            </td>
        </tr>
        <tr>
            <td>
                <h6>
                    <strong>Source of Funding</strong>
                </h6>
                @if($request->funding_source != $history->funding_source)
                    <p style="color: #bd564f"><em><small>Before: {{ $history->funding_source }}</small></em></p>
                @endif
                <span>{{ $request->funding_source }}</span>
            </td>
        </tr>
        <tr>
            <td>
                <h6>
                    <strong>Purpose of Trip (i.e name(s) of event(s), etc.)</strong>
                </h6>
                @if($request->purpose != $history->purpose)
                    <p style="white-space:pre-wrap; word-wrap:break-word; color: #bd564f"><em><small>Before: {{ $history->purpose }}</small></em></p>
                @endif
                <span style="white-space:pre-wrap; word-wrap:break-word">{{ $request->purpose }}</span>
            </td>
        </tr>
        <tr>
            <td>
                <h6>
                    <strong>Coverage of Classes and/or Other Duties (please indicate faculty member
                        acting on your behalf as Head during your absence)</strong>
                </h6>
                @if($request->coverage != $history->coverage)
                    <p style="white-space:pre-wrap; word-wrap:break-word; color: #bd564f"><em><small>Before: {{ $history->coverage }}</small></em></p>
                @endif
                <span style="white-space:pre-wrap; word-wrap:break-word">{{ $request->coverage }}</span>
            </td>
        </tr>
        @php
            $date = new \DateTime("now", new \DateTimeZone('America/Chicago'));
            $currentDate = $date->format('Y-m-d');

            if ($request->departure_date < $currentDate) {
                $expired = true;
            }
            else {
                $expired = false;
            }
        @endphp
        @if(!is_null($request->status))
            <tr>
                <td>
                    <h6>
                        @if($request->user->role == 2)
                            <strong>Dean's Comments</strong>
                        @elseif($request->user->role == 3)
                            <strong>Head of Department's Comments</strong>
                        @endif
                    </h6>
                    <span
                        style="white-space:pre-wrap; word-wrap:break-word">{{ $request->comments }}</span>
                </td>
            </tr>
            <tr>
                <td>
                    <h6>
                        <strong>Decision</strong>
                    </h6>
                    @if($request->status == '1')
                        <span>Approved</span>
                    @else
                        <span>Denied</span>
                    @endif
                </td>
            </tr>
        @endif
        </tbody>
    </table>
    <div>
        <label class="mt-2" for="comments"><strong>Your Previous Comments</strong></label>
        @if(!is_null($history->comments))
            <textarea type="text" rows="3"
                      class="form-control @error('comments') is-invalid @enderror"
                      id="comments" disabled placeholder="">{{ $history->comments }}</textarea>
        @endif
    </div>
    @if(Auth::user()->role == $request->user->role && !is_null($request->status) && $request->status != 1)
        <a href="/request/{{$request->id}}/edit">
            <button class="align-content-center btn btn-secondary btn-md"
                    type="button">Edit and Request again
            </button>
        </a>
    @endif

    @if(is_null($request->status) && Auth::user()->role == ($request->user->role - 1) && !$expired)
        <form class="input-fields" id="request" action="/request/{{$request->id}}"
              enctype="multipart/form-data"
              method="post">
            @csrf
            @method('PATCH')
            <div>
                <label class="mt-3" for="comments"><strong>Comments</strong></label>
                <textarea type="text" rows="3"
                          class="form-control @error('comments') is-invalid @enderror" name="comments"
                          id="comments" placeholder=""></textarea>
                @error('comments')
                <span class="invalid-feedback" role="alert">
                    <strong>Please provide your comments</strong>
                </span>
                @enderror
            </div>
            <button class="mt-2 align-content-center btn btn-success btn-md" name="status" value="1"
                    type="submit">Approve
            </button>
            <button class="ml-2 mt-2 align-content-center btn btn-secondary btn-md" name="status"
                    value="0" type="submit">Deny
            </button>
        </form>
    @endif
@endsection
