@extends('layout')

@section('scripts')
    {{ HTML::script('js/requests/history-denied.js') }}
@endsection

@section('title', 'My Denied Requests')

@section('My Requests', 'active')

@section('content')
    <nav>
        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
            <a class="nav-item nav-link" id="nav-pending-tab" data-toggle="tab" href="#nav-pending"
               role="tab" aria-controls="nav-pending" aria-selected="false"><span
                    style="color: #0455A4;">Pending</span></a>
            <a class="nav-item nav-link" id="nav-approved-tab" data-toggle="tab" href="#nav-approved"
               role="tab" aria-controls="nav-approved" aria-selected="false"><span
                    style="color: #0455A4;">Approved</span></a>
            <a class="nav-item nav-link active" id="nav-denied-tab" data-toggle="tab" href="#nav-denied"
               role="tab" aria-controls="nav-denied" aria-selected="true"><span
                    style="color: #0455A4;">Denied</span></a>
            <a class="nav-item nav-link" id="nav-expired-tab" data-toggle="tab" href="#nav-expired"
               role="tab" aria-controls="nav-expired" aria-selected="false"><span
                    style="color: #0455A4;">Expired</span></a>
        </div>
    </nav>
    <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
        <div class="tab-pane fade" id="nav-pending" role="tabpanel"
             aria-labelledby="nav-pending-tab"></div>
        <div class="tab-pane fade" id="nav-approved" role="tabpanel"
             aria-labelledby="nav-approved-tab"></div>
        <div class="tab-pane fade show active" id="nav-denied" role="tabpanel"
             aria-labelledby="nav-denied-tab">
            @if(count($requests) >= 1)
                <table class="table table-striped table-responsive w-100 d-block d-md-table">
                    <thead>
                    <tr>
                        <th scope="col">Request Date</th>
                        <th scope="col">Destination</th>
                        <th scope="col">Departure Date</th>
                        @if(Auth::user()->role == 2)
                            <th scope="col">Dean's Comments</th>
                        @else
                            <th scope="col">HOD's Comments</th>
                        @endif
                        <th width="20%" scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($requests as $request)
                        <tr>
                            <td>
                                {{ date("F j, Y", strtotime($request->request_date)) }}
                            </td>

                            <td>
                                {{ $request->destination }}
                            </td>

                            <td>
                                {{ date("F j, Y", strtotime($request->departure_date)) }}
                            </td>

                            <td>
                                <p class="truncate">{{ $request->comments }}</p>
                            </td>

                            <td>
                                <div class="row d-flex">
                                    <a href="/request/{{$request->id}}">
                                        <button class="align-content-center btn btn-primary btn-md"
                                                type="button">View
                                        </button>
                                    </a>
                                    <a class="pl-2" href="/request/{{$request->id}}/edit">
                                        <button class="align-content-center btn btn-secondary btn-md"
                                                type="button">Edit
                                        </button>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-12 d-flex justify-content-center pt-4">
                        {{ $requests->links() }}
                    </div>
                </div>
            @else
                <div class="mt-5 text-center">You have no denied requests!</div>
            @endif
        </div>
        <div class="tab-pane fade" id="nav-expired" role="tabpanel"
             aria-labelledby="nav-expired-tab"></div>
    </div>
@endsection
