@extends('layout')

@section('scripts')
    {{ HTML::script('js/all-approved-requests.js') }}
@endsection

@section('title', 'All Approved Requests')

@section('Approved Requests', 'active')

@section('content')
    @php
        $selected_year = app('request')->input('year')
    @endphp
    <label for="exampleFormControlSelect2">Filter by year:</label>
    <select id="yearFilter" id="exampleFormControlSelect2">

        <option selected value="">{{ $selected_year }}</option>
        @foreach ($years as $year)
            @if($year->year != $selected_year)
                <option value="?year={{$year->year}}">{{$year->year}}</option>
            @endif
        @endforeach
        <option value="/">Reset</option>
    </select>
    <table class="mt-3 table table-striped table-responsive w-100 d-block d-md-table">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Destination</th>
            <th scope="col">Departure Date</th>
            <th scope="col" width="30%">Comments</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @if(count($requests) >= 1)
            @foreach ($requests as $request)
                <tr>
                    <td>
                        {{ $request->name . ' (' . $request->email . ')'}}
                    </td>

                    <td>
                        {{ $request->destination }}
                    </td>

                    <td>
                        {{ date("F j, Y", strtotime($request->departure_date)) }}
                    </td>
                    <td>
                        <p class="truncate">{{ $request->comments }}</p>
                    </td>

                    <td>
                        <a href="/request/{{$request->id}}">
                            <button class="align-content-center btn btn-primary btn-md"
                                    name="status" value="0" type="button">View
                            </button>
                        </a>
                    </td>

                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
    <div class="row">
        <div class="col-12 d-flex justify-content-center pt-4">
            {{ $requests->links() }}
        </div>
    </div>
    <footer class="my-5 pt-5 text-muted text-center text-small">
    </footer>
@endsection
