<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>UIC-COE Travel Form</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    {{ HTML::style('css/global.css') }}
    @yield('scripts')
</head>

<body>
<div class="d-flex" id="wrapper">
    <div id="page-content-wrapper">
        <!-- Nav Bar -->
        <nav class="navbar navbar-expand-md navbar-light bg-light border-bottom">
            <a class="navbar-brand" href="http://engineering.uic.edu">
                <img src="{{ asset('images/COL.ENG.MAIN.LOCKB.SM.RED.PNG') }}" alt="" style="width:200px;"/>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                @if (Route::has('login'))
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        @auth
                            @if( Auth::user()->role != 1 && Auth::user()->role != 4)
                                <li class="nav-item">
                                    <a class="nav-link @yield('New Request')" href="{{ url("/request/new") }}">New
                                        request</a>
                                </li>
                            @endif
                            @if( Auth::user()->role != 3 && Auth::user()->role != 4)
                                <li class="nav-item">
                                    <a class="nav-link @yield('Pending Approvals')" href="{{ url('/') }}">Pending
                                        approvals</a>
                                </li>
                            @endif
                            @if( Auth::user()->role != 1 && Auth::user()->role != 4)
                                <li class="nav-item">
                                    <a class="nav-link @yield('My Requests')"
                                       href="{{ url("/requests/history/pending") }}">My requests</a>
                                </li>
                            @endif
                            @if( Auth::user()->role == 4)
                                <li class="nav-item">
                                    <a class="nav-link @yield('Approved Requests')"
                                       href="{{ url("/ApprovedRequests") }}">Approved Requests</a>
                                </li>
                            @endif
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}">Logout</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">Login</a></a>
                            </li>
                        @endauth
                    </ul>
                @endif
            </div>
        </nav>

        <div class="container">
            <div class="py-5 text-center">
                <h2 class="display-5">@yield('title')</h2>
            </div>
            <div class="row">
                <div class="mt-3 col-md-10 mx-auto">
                    @yield('content')

                    <footer class="my-5 pt-5 text-muted text-center text-small">
                    </footer>

                </div>
            </div>

        </div>
    </div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>

</html>
