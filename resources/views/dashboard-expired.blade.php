@extends('layout')

@section('scripts')
    {{ HTML::script('js/dashboard-expired.js') }}
@endsection

@section('title', 'Expired Requests')

@section('Pending Approvals', 'active')

@section('content')
    <nav>
        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
            <a class="nav-item nav-link" id="nav-pending-tab" data-toggle="tab" href="#nav-pending"
               role="tab" aria-controls="nav-pending" aria-selected="false"><span
                    style="color: #0455A4;">Pending</span></a>
            <a class="nav-item nav-link" id="nav-approved-tab" data-toggle="tab" href="#nav-approved"
               role="tab" aria-controls="nav-approved" aria-selected="false"><span
                    style="color: #0455A4;">Approved</span></a>
            <a class="nav-item nav-link" id="nav-denied-tab" data-toggle="tab" href="#nav-denied"
               role="tab" aria-controls="nav-denied" aria-selected="false"><span
                    style="color: #0455A4;">Denied</span></a>
            <a class="nav-item nav-link active" id="nav-expired-tab" data-toggle="tab" href="#nav-expired"
               role="tab" aria-controls="nav-expired" aria-selected="true"><span
                    style="color: #0455A4;">Expired</span></a>
        </div>
    </nav>
    <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
        <div class="tab-pane fade" id="nav-pending" role="tabpanel"
             aria-labelledby="nav-pending-tab"></div>
        <div class="tab-pane fade" id="nav-approved" role="tabpanel"
             aria-labelledby="nav-approved-tab"></div>
        <div class="tab-pane fade" id="nav-denied" role="tabpanel"
             aria-labelledby="nav-denied-tab"></div>
        <div class="tab-pane fade show active" id="nav-expired" role="tabpanel"
             aria-labelledby="nav-expired-tab">
            @if(count($requests) >= 1)
                <table class="table table-striped table-responsive w-100 d-block d-md-table">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Destination</th>
                        <th scope="col">Departure Date</th>
                        <th width="25%" scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($requests as $request)
                        <tr>
                            <td>
                                {{ $request->name . ' (' . $request->email . ')'}}
                            </td>

                            <td>
                                {{ $request->destination }}
                            </td>

                            <td>
                                {{ date("F j, Y", strtotime($request->departure_date)) }}
                            </td>

                            <td>
                                <div class="row">
                                    <div class="d-flex">
                                        <div>
                                            @if($request->modified == 1)
                                                <a href="/request/edited/{{$request->id}}">
                                                    <button class="mr-5 align-content-center btn btn-primary btn-md"
                                                            type="button">View
                                                    </button>
                                                </a>
                                            @else
                                                <a href="/request/{{$request->id}}">
                                                    <button class="mr-5 align-content-center btn btn-primary btn-md"
                                                            type="button">View
                                                    </button>
                                                </a>
                                            @endif
                                        </div>
                                        <div>
                                            <form class="input-fields" id="request"
                                                  action="/request/{{$request->id}}/delete"
                                                  enctype="multipart/form-data"
                                                  method="post">
                                                @csrf
                                                <button class="ml-5 align-content-center btn btn-md"
                                                        data-toggle="tooltip" data-placement="right"
                                                        title="Delete"
                                                        type="submit"><i class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-12 d-flex justify-content-center pt-4">
                        {{ $requests->links() }}
                    </div>
                </div>
            @else
                <div class="mt-5 text-center">There are no expired requests!</div>
            @endif
        </div>
        <footer class="my-5 pt-5 text-muted text-center text-small">
        </footer>
    </div>
@endsection

