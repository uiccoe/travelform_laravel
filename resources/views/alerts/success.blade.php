@if(session('success'))
    <div class="alert alert-success" role="alert">
        <a href="" class="close" data-dismiss="alert">&times;</a>
        {{session('success')}}
    </div>
    <script>
        setTimeout(function () {
            $(".alert").hide("fade");
        }, 5000);
    </script>
@endif
