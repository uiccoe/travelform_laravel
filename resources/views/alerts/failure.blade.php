@if(session('failure'))
    <div class="alert alert-danger" role="alert">
        <a href="" class="close" data-dismiss="alert">&times;</a>
        {{session('failure')}}
    </div>
    <script>
        setTimeout(function () {
            $(".alert").hide("fade");
        }, 5000);
    </script>
@endif
