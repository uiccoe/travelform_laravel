@component('mail::message')
Greetings,

You have request(s) waiting for your decision that were due to depart yesterday.

Click on the link below to see the requests:
@component('mail::button', ['url' => env('APP_URL')])
UIC-COE Travel Form
@endcomponent

Thank you.
@endcomponent
