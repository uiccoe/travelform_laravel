@component('mail::message')
Greetings,

@if ($decision)
You have approved the following faculty's travel request:
@else
You have denied the following faculty's travel request:
@endif

Name : {!!  $faculty->name !!}
<br>
NetID : {!! $faculty->netid !!}
<br>
Date: {!! Carbon\Carbon::parse($submitDate)->setTimezone('America/Chicago')->format('D, M j Y, g:i:s A T') !!}
<br>

Thank you.
@endcomponent
