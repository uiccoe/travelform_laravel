@component('mail::message')
Dear {!! $faculty !!},

@if ($decision)
{!! $supervisor !!} has approved your travel request on {!!  Carbon\Carbon::parse($submitDate)->setTimezone('America/Chicago')->format('D, M j Y, g:i:s A T') !!}.
@else
{!! $supervisor !!} has denied your travel request on {!! Carbon\Carbon::parse($submitDate)->setTimezone('America/Chicago')->format('D, M j Y, g:i:s A T') !!}.
@endif

Click on the link below to see your request:
@component('mail::button', ['url' => env('APP_URL')])
UIC-COE Travel Form
@endcomponent

Thank you.
@endcomponent
