@component('mail::message')
Greetings,

{!! $supervisor->name !!} has approved the travel request of {!! $faculty->name !!} on {!! Carbon\Carbon::parse($submitDate)->setTimezone('America/Chicago')->format('D, M j Y, g:i:s A T') !!}.

Click on the link below to see the request:

@component('mail::button', ['url' => env('APP_URL')])
UIC-COE Travel Form
@endcomponent


Thank you.
@endcomponent
