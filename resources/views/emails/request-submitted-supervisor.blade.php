@component('mail::message')
Greetings,

The following faculty has submitted a travel request.

<strong>Name :</strong> {!! $user->name !!}
<br>
<strong>NetID :</strong> {!! $user->netid !!}
<br>
<strong>Date of Submission
:</strong> {!! Carbon\Carbon::parse($submitDate)->setTimezone('America/Chicago')->format('D, M j Y, g:i:s A T') !!}

Click on the link below to see the submission:

@component('mail::button', ['url' => env('APP_URL')])
UIC-COE Travel Form
@endcomponent


Thank you.
@endcomponent
