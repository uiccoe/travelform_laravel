@component('mail::message')
Dear {{ $user->name }},

Thank you for submitting your request. Your submission has been recorded on
{!! Carbon\Carbon::parse($submitDate)->setTimezone('America/Chicago')->format('D, M j Y, g:i:s A T') !!}. Click on the link below to see your
submission:

@component('mail::button', ['url' => env('APP_URL')])
UIC-COE Travel Form
@endcomponent

Thank you.
@endcomponent
