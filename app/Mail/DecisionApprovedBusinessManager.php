<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DecisionApprovedBusinessManager extends Mailable
{
    use Queueable, SerializesModels;

    public $faculty;
    public $supervisor;
    public $submitDate;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($faculty, $supervisor, $submitDate)
    {
        $this->faculty = $faculty;
        $this->supervisor = $supervisor;
        $this->submitDate = $submitDate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Travel request approved by ' . $this->supervisor->name)->markdown('emails.decision-approved-business-manager');
    }
}
