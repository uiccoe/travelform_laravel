<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestSubmittedSupervisor extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $submitDate;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $submitDate)
    {
        $this->user = $user;
        $this->submitDate = $submitDate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Travel request submitted by ' . $this->user->name)->markdown('emails.request-submitted-supervisor');
    }
}
