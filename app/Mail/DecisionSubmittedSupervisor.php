<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DecisionSubmittedSupervisor extends Mailable
{
    use Queueable, SerializesModels;

    public $faculty;
    public $decision;
    public $submitDate;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($faculty, $decision, $submitDate)
    {
        $this->faculty = $faculty;
        $this->decision = $decision;
        $this->submitDate = $submitDate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Travel request decision submission successful')->markdown('emails.decision-submitted-supervisor');
    }
}