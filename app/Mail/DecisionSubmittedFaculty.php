<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DecisionSubmittedFaculty extends Mailable
{
    use Queueable, SerializesModels;

    public $faculty;
    public $supervisor;
    public $decision;
    public $submitDate;

    /**
     * Create a new message instance.
     *
     * @return void
     * $request->user, Auth::user()->name, $data['status'], $$statusDate)
     */
    public function __construct($faculty, $supervisor, $decision, $submitDate)
    {
        $this->faculty = $faculty;
        $this->supervisor = $supervisor;
        $this->decision = $decision;
        $this->submitDate = $submitDate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Travel request decision made by ' . $this->supervisor)->markdown('emails.decision-submitted-faculty');
    }
}