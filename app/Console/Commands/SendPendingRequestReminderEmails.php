<?php

namespace App\Console\Commands;

use App\Mail\PendingRequestsReminder;
use App\User;
use DateInterval;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SendPendingRequestReminderEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:pending-requests-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminders about pending requests once departure date crosses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = new \DateTime("now", new \DateTimeZone('America/Chicago'));
        $yesterday = $date->sub(new DateInterval('P1D'));
        $yesterday = $yesterday->format('Y-m-d');

        $users = DB::table('proposals')
            ->join('users', 'proposals.user_id', '=', 'users.id')
            ->whereNull('proposals.status')
            ->where('proposals.departure_date', '=', $yesterday)
            ->select('users.dept', 'users.role')
            ->distinct()
            ->get();


        foreach ($users as $user) {
            if ($user->role == 3) {
                $supervisor = User::where('role', 2)->where('dept', $user->dept)->first();
            } else {
                $supervisor = User::where('role', 1)->first();
            }
            Mail::to($supervisor->email)->send(new PendingRequestsReminder());
        }

    }
}
