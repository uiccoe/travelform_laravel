<?php

namespace App\Http\Middleware;

use Closure;

class RedirectOnLogin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->role == 4) {
            return redirect("/ApprovedRequests");
        } else if ($request->user()->role == 3) {
            return redirect("/requests/history/pending");
        }

        return $next($request);
    }
}
