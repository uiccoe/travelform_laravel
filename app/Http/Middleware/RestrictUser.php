<?php

namespace App\Http\Middleware;

use App\Proposal;
use Closure;

class RestrictUser
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $proposal = Proposal::findorFail($request->route('id'));


        if ($request->user()->role + 1 == $proposal->user->role && ($request->user()->dept == $proposal->user->dept || $request->user()->role == 1)) {
            return $next($request);
        } else if ($request->user()->role == 4 && $request->user()->dept == $proposal->user->dept && $proposal->status == 1) {
            return $next($request);
        } else if ($request->user()->role == $proposal->user->role) {
            return $next($request);
        }

        return redirect('/');
    }
}
