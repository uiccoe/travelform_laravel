<?php

namespace App\Http\Middleware;

use App\Proposal;
use Closure;

class RestrictEditedRequestsView
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $proposal = Proposal::findorFail($request->route('id'));

        if ($request->user()->role + 1 == $proposal->user->role && ($request->user()->dept == $proposal->user->dept || $request->user()->role == 1)) {
            if (is_null($proposal->status)) {
                return $next($request);
            }
        }
        return redirect('/');
    }
}
