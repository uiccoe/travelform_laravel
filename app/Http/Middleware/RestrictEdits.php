<?php

namespace App\Http\Middleware;

use App\Proposal;
use Closure;

class RestrictEdits
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $proposal = Proposal::findorFail($request->route('id'));

        if ($proposal->user_id != $request->user()->id) {
            return redirect('/');
        }
        else {
            if (is_null($proposal->status) ||  $proposal->status==1) {
                return redirect('/');
            }
        }
        return $next($request);
    }
}
