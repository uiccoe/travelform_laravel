<?php

namespace App\Http\Middleware;

use Closure;

class RestrictBusinessManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->route()->getActionName() == "App\Http\Controllers\ProposalController@getAllApprovedRequests") {
            if ($request->user()->role != 4) {
                return redirect("/");
            } else {
                return $next($request);
            }
        } else {
            if ($request->user()->role == 4) {
                return redirect("/ApprovedRequests");
            }
            else if ($request->user()->role == 1){
                return redirect("/");
            }
            else {
                return $next($request);
            }
        }

    }

}
