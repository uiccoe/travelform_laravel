<?php

namespace App\Http\Controllers;

use App\Mail\DecisionApprovedBusinessManager;
use App\Mail\DecisionSubmittedFaculty;
use App\Mail\DecisionSubmittedSupervisor;
use App\Mail\RequestSubmittedFaculty;
use App\Mail\RequestSubmittedSupervisor;
use App\Proposal;
use App\ProposalHistory;
use App\User;
use DateInterval;
use Illuminate\Http\Request;
use DateTime;
use Auth;
use DB;
use Illuminate\Support\Facades\Mail;

class ProposalController extends Controller
{
    public function insertNewRequest()
    {
        $data = request()->validate([
            'destination' => 'required',
            'departure_date' => 'required',
            'duration' => 'required',
            'expense_amount' => '',
            'funding_source' => '',
            'purpose' => 'required',
            'coverage' => '',
        ]);

        $date = new \DateTime("now", new \DateTimeZone('America/Chicago'));
        $requestDate = $date->format('Y-m-d');

        $originalDate = request('departure_date');
        $departureDate = date("Y-m-d", strtotime($originalDate));

        auth()->user()->proposals()->create(array_merge($data, ['request_date' => $requestDate, 'departure_date' => $departureDate]));
        $submitDate = date('Y-m-d H:i:s');

        if (auth()->user()->role == 3) {
            $supervisor = User::where('role', 2)->where('dept', auth()->user()->dept)->first();
        } else {
            $supervisor = User::where('role', 1)->first();
        }

        Mail::to(Auth::user()->email)->send(new RequestSubmittedFaculty(auth()->user(), $submitDate));
        Mail::to($supervisor->email)->send(new RequestSubmittedSupervisor(auth()->user(), $submitDate));

        return back()->with('success', 'Your request has been submitted. A confirmation has been sent to your email');
    }

    public function edit($proposal_id)
    {
        $proposal = Proposal::findOrFail($proposal_id);

        return view('requests.edit', [
            'request' => $proposal,
        ]);
    }

    public function updateRequest($proposal_id)
    {
        $proposal = Proposal::findOrFail($proposal_id);

        $data = request()->validate([
            'destination' => 'required',
            'departure_date' => 'required',
            'duration' => 'required',
            'expense_amount' => '',
            'funding_source' => '',
            'purpose' => 'required',
            'coverage' => '',
        ]);

        $date = new \DateTime("now", new \DateTimeZone('America/Chicago'));
        $requestDate = $date->format('Y-m-d');


        $originalDate = request('departure_date');
        $departureDate = date("Y-m-d", strtotime($originalDate));

        $updatedData = array_merge($data, ['departure_date' => $departureDate]);
        $proposal->fill($updatedData);

        if ($proposal->isDirty()) {

            $proposal = Proposal::findOrFail($proposal_id);
            if ($proposal->modified == 1) {
                $history = ProposalHistory::where('proposal_id', $proposal->id)->first();

            } else {
                $history = new ProposalHistory();
            }

            $history->proposal_id = $proposal->id;
            $history->destination = $proposal->destination;
            $history->request_date = $proposal->request_date;
            $history->departure_date = $proposal->departure_date;
            $history->duration = $proposal->duration;
            $history->expense_amount = $proposal->expense_amount;
            $history->funding_source = $proposal->funding_source;
            $history->purpose = $proposal->purpose;
            $history->coverage = $proposal->coverage;
            $history->comments = $proposal->comments;

            $history->save();

            $proposal->update(array_merge($data, ['status_date' => null, 'request_date' => $requestDate, 'departure_date' => $departureDate, 'status' => null, 'modified' => 1]));

            $submitDate = date('Y-m-d H:i:s');

            if (auth()->user()->role == 3) {
                $supervisor = User::where('role', 2)->where('dept', auth()->user()->dept)->first();
            } else {
                $supervisor = User::where('role', 1)->first();
            }

            Mail::to(Auth::user()->email)->send(new RequestSubmittedFaculty(auth()->user(), $submitDate));
            Mail::to($supervisor->email)->send(new RequestSubmittedSupervisor(auth()->user(), $submitDate));

            return redirect("/requests/history/pending")->with('success', 'Your request has been edited and submitted again. A confirmation has been sent to your email');
        } else {
            return back()->with('failure', 'Request not submitted. No changes were made.');
        }
    }

    public function showRequest($proposal_id)
    {
        $proposal = Proposal::findOrFail($proposal_id);
        return view('requests.show', [
            'request' => $proposal,
        ]);
    }

    public function submitDecision($request_id)
    {
        $request = Proposal::findOrFail($request_id);
        if (request('status') == 0) {
            $data = request()->validate([
                'comments' => 'required',
                'status' => 'required',
            ]);
        } elseif (request('status') == 1) {
            $data = request()->validate([
                'comments' => '',
                'status' => 'required',
            ]);
        }

        $statusDate = new DateTime();

        $request->update(array_merge($data, ['status_date' => $statusDate]));
        $faculty = $request->user;

        Mail::to($faculty->email)->send(new DecisionSubmittedFaculty($faculty->name, Auth::user()->name, $data['status'], $statusDate));
        Mail::to(Auth::user()->email)->send(new DecisionSubmittedSupervisor($faculty, $data['status'], $statusDate));

        if ($data['status'] == 1) {
            $businessManager = User::where('role', 4)->where('users.dept', Auth::user()->dept)->first();
            if ($businessManager != null) {
                Mail::to($businessManager->email)->send(new DecisionApprovedBusinessManager($faculty, Auth::user(), $statusDate));
            }
        }
        return redirect("/")->with('success', 'Your decision has been submitted. A confirmation has been sent to user\'s email');

    }

    public function showEditedRequest($proposal_id)
    {
        $proposal = Proposal::findOrFail($proposal_id);
        $history = ProposalHistory::where('proposal_id', $proposal->id)->first();
        return view('requests.show-edited', [
            'request' => $proposal,
            'history' => $history,
        ]);
    }

    public function getPendingRequests()
    {

        $date = new \DateTime("now", new \DateTimeZone('America/Chicago'));
        $sevenDaysAgo = $date->sub(new DateInterval('P7D'));
        $sevenDaysAgo = $sevenDaysAgo->format('Y-m-d');

        if (Auth::user()->role == 1) {
            $pendingRequests = DB::table('proposals')
                ->join('users', 'proposals.user_id', '=', 'users.id')
                ->whereNull('proposals.status')
                ->where('users.role', 2)
                ->where('proposals.departure_date', '>=', $sevenDaysAgo)
                ->select('proposals.*', 'users.name', 'users.email')
                ->orderBy('proposals.departure_date', 'ASC')
                ->paginate(5);
        } elseif (Auth::user()->role == 2) {
            $pendingRequests = DB::table('proposals')
                ->join('users', 'proposals.user_id', '=', 'users.id')
                ->whereNull('proposals.status')
                ->where('users.dept', Auth::user()->dept)
                ->where('users.role', 3)
                ->where('proposals.departure_date', '>=', $sevenDaysAgo)
                ->select('proposals.*', 'users.name', 'users.email')
                ->orderBy('proposals.departure_date', 'ASC')
                ->paginate(5);
        }

        return view('dashboard-pending', [
            'requests' => $pendingRequests,
        ]);
    }

    public function getApprovedRequests()
    {
        if (Auth::user()->role == 1) {
            $approvedRequests = DB::table('proposals')
                ->join('users', 'proposals.user_id', '=', 'users.id')
                ->where('status', 1)
                ->where('users.role', 2)
                ->select('proposals.*', 'users.name', 'users.email')
                ->orderBy('proposals.departure_date', 'ASC')
                ->paginate(5);
        } elseif (Auth::user()->role == 2) {
            $approvedRequests = DB::table('proposals')
                ->join('users', 'proposals.user_id', '=', 'users.id')
                ->where('status', 1)
                ->where('users.dept', Auth::user()->dept)
                ->where('users.role', 3)
                ->select('proposals.*', 'users.name', 'users.email')
                ->orderBy('proposals.departure_date', 'ASC')
                ->paginate(5);
        }

        return view('dashboard-approved', [
            'requests' => $approvedRequests,
        ]);
    }


    public function getDeniedRequests()
    {
        if (Auth::user()->role == 1) {

            $deniedRequests = DB::table('proposals')
                ->join('users', 'proposals.user_id', '=', 'users.id')
                ->where('status', 0)
                ->where('users.role', 2)
                ->select('proposals.*', 'users.name', 'users.email')
                ->orderBy('proposals.departure_date', 'ASC')
                ->paginate(5);
        } elseif (Auth::user()->role == 2) {
            $deniedRequests = DB::table('proposals')
                ->join('users', 'proposals.user_id', '=', 'users.id')
                ->where('status', 0)
                ->where('users.dept', Auth::user()->dept)
                ->where('users.role', 3)
                ->select('proposals.*', 'users.name', 'users.email')
                ->orderBy('proposals.departure_date', 'ASC')
                ->paginate(5);
        }

        return view('dashboard-denied', [
            'requests' => $deniedRequests,
        ]);
    }

    public function getExpiredRequests()
    {
        $date = new \DateTime("now", new \DateTimeZone('America/Chicago'));
        $sevenDaysAgo = $date->sub(new DateInterval('P7D'));
        $sevenDaysAgo = $sevenDaysAgo->format('Y-m-d');

        if (Auth::user()->role == 1) {
            $expiredRequests = DB::table('proposals')
                ->join('users', 'proposals.user_id', '=', 'users.id')
                ->whereNull('proposals.status')
                ->where('users.role', 2)
                ->where('proposals.departure_date', '<', $sevenDaysAgo)
                ->where('proposals.deleted_at', null)
                ->select('proposals.*', 'users.name', 'users.email')
                ->orderBy('proposals.departure_date', 'ASC')
                ->paginate(5);
        } elseif (Auth::user()->role == 2) {
            $expiredRequests = DB::table('proposals')
                ->join('users', 'proposals.user_id', '=', 'users.id')
                ->whereNull('proposals.status')
                ->where('users.dept', Auth::user()->dept)
                ->where('users.role', 3)
                ->where('proposals.departure_date', '<', $sevenDaysAgo)
                ->where('proposals.deleted_at', null)
                ->select('proposals.*', 'users.name', 'users.email')
                ->orderBy('proposals.departure_date', 'ASC')
                ->paginate(5);
        }

        return view('dashboard-expired', [
            'requests' => $expiredRequests,
        ]);
    }


    public function getUserPendingRequests()
    {
        $date = new \DateTime("now", new \DateTimeZone('America/Chicago'));
        $sevenDaysAgo = $date->sub(new DateInterval('P7D'));
        $sevenDaysAgo = $sevenDaysAgo->format('Y-m-d');

        $pendingRequests = DB::table('proposals')
            ->join('users', 'proposals.user_id', '=', 'users.id')
            ->whereNull('proposals.status')
            ->where('users.id', Auth::user()->id)
            ->where('proposals.departure_date', '>=', $sevenDaysAgo)
            ->select('proposals.*', 'users.name', 'users.email')
            ->orderBy('proposals.request_date', 'DESC')
            ->paginate(5);

        return view('requests.history-pending', [
            'requests' => $pendingRequests,
        ]);
    }


    public function getUserApprovedRequests()
    {
        $approvedRequests = DB::table('proposals')
            ->join('users', 'proposals.user_id', '=', 'users.id')
            ->where('status', 1)
            ->where('users.id', Auth::user()->id)
            ->select('proposals.*', 'users.name', 'users.email')
            ->orderBy('proposals.request_date', 'DESC')
            ->paginate(5);
        return view('requests.history-approved', [
            'requests' => $approvedRequests,
        ]);
    }


    public function getUserDeniedRequests()
    {
        $deniedRequests = DB::table('proposals')
            ->join('users', 'proposals.user_id', '=', 'users.id')
            ->where('status', 0)
            ->where('users.id', Auth::user()->id)
            ->select('proposals.*', 'users.name', 'users.email')
            ->orderBy('proposals.request_date', 'DESC')
            ->paginate(5);

        return view('requests.history-denied', [
            'requests' => $deniedRequests,
        ]);
    }

    public function getUserExpiredRequests()
    {
        $date = new \DateTime("now", new \DateTimeZone('America/Chicago'));
        $sevenDaysAgo = $date->sub(new DateInterval('P7D'));
        $sevenDaysAgo = $sevenDaysAgo->format('Y-m-d');

        $expiredRequests = DB::table('proposals')
            ->join('users', 'proposals.user_id', '=', 'users.id')
            ->whereNull('proposals.status')
            ->where('users.id', Auth::user()->id)
            ->where('proposals.departure_date', '<', $sevenDaysAgo)
            ->where('proposals.deleted_at', null)
            ->select('proposals.*', 'users.name', 'users.email')
            ->orderBy('proposals.request_date', 'DESC')
            ->paginate(5);

        return view('requests.history-expired', [
            'requests' => $expiredRequests,
        ]);
    }

    public function getAllApprovedRequests()
    {
        $yearsList = DB::select("SELECT YEAR(departure_date) AS year from proposals group by YEAR(departure_date)");
        $years = array_reverse($yearsList);
        if (request()->has('year')) {
            $allApprovedRequests = DB::table('proposals')
                ->join('users', 'proposals.user_id', '=', 'users.id')
                ->where('users.dept', Auth::user()->dept)
                ->where('status', 1)
                ->select('proposals.*', 'users.name', 'users.email')
                ->whereRaw('YEAR(proposals.departure_date)=?', [request('year')])
                ->orderBy('proposals.departure_date', 'DESC')
                ->paginate(25);

        } else {
            $allApprovedRequests = DB::table('proposals')
                ->join('users', 'proposals.user_id', '=', 'users.id')
                ->where('users.dept', Auth::user()->dept)
                ->where('status', 1)
                ->select('proposals.*', 'users.name', 'users.email')
                ->orderBy('proposals.departure_date', 'DESC')
                ->paginate(25);
        }

        return view('all-approved-requests', [
            'requests' => $allApprovedRequests,
            'years'=>$years,
        ]);
    }

    public function delete($proposal_id)
    {
        $proposal = Proposal::findOrFail($proposal_id);
        $proposal->delete();
        return redirect()->back();
    }

}
