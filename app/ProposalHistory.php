<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProposalHistory extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(Proposal::class);
    }
}
