<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('login')->get('/login', '\\' . Route::getRoutes()->getByName('shibboleth-login')->getActionName());
Route::name('logout')->get('/logout', '\\' . Route::getRoutes()->getByName('shibboleth-logout')->getActionName());


Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'ProposalController@getPendingRequests')->middleware('redirectOnLogin');
    Route::get('/approved', 'ProposalController@getApprovedRequests')->middleware('redirectOnLogin');
    Route::get('/denied', 'ProposalController@getDeniedRequests')->middleware('redirectOnLogin');
    Route::get('/expired', 'ProposalController@getExpiredRequests')->middleware('redirectOnLogin');


    Route::get('/request/new', function () {
        return view('requests.create');
    })->middleware('restrictBusinessManager');
    Route::patch('/request/{id}/edit/submit', 'ProposalController@updateRequest')->middleware('restrictEdits');

    Route::get('/request/{id}/edit', 'ProposalController@edit')->middleware('restrictEdits');



    Route::get('/requests/history/pending', 'ProposalController@getUserPendingRequests')->middleware('restrictBusinessManager');
    Route::get('/requests/history/approved', 'ProposalController@getUserApprovedRequests')->middleware('restrictBusinessManager');
    Route::get('/requests/history/denied', 'ProposalController@getUserDeniedRequests')->middleware('restrictBusinessManager');
    Route::get('/requests/history/expired', 'ProposalController@getUserExpiredRequests')->middleware('restrictBusinessManager');


    Route::get('/ApprovedRequests', 'ProposalController@getAllApprovedRequests')->middleware('restrictBusinessManager');

    Route::post('/request/submit', 'ProposalController@insertNewRequest');

    Route::post('/request/{id}/delete', 'ProposalController@delete');

    Route::get('/request/{id}', 'ProposalController@showRequest')->middleware('restrictUser');
    Route::get('/request/edited/{id}', 'ProposalController@showEditedRequest')->middleware('restrictEditedRequestsView');
    Route::patch('/request/{id}', 'ProposalController@submitDecision');

});
