<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('destination', '100');
            $table->date('request_date');
            $table->date('departure_date');
            $table->unsignedSmallInteger('duration');
            $table->decimal('expense_amount', 8, 2)->nullable();
            $table->string('funding_source', '100')->nullable();
            $table->text('purpose');
            $table->text('coverage')->nullable();
            $table->unsignedSmallInteger('status')->nullable();
            $table->dateTime('status_date')->nullable();
            $table->text('comments')->nullable();
            $table->unsignedSmallInteger('modified')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals');
    }
}
