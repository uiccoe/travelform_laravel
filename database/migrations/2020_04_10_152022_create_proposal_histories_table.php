<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposal_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('proposal_id');
            $table->string('destination', '100');
            $table->date('request_date');
            $table->date('departure_date');
            $table->unsignedSmallInteger('duration');
            $table->decimal('expense_amount', 8, 2)->nullable();
            $table->string('funding_source', '100')->nullable();
            $table->text('purpose');
            $table->text('coverage')->nullable();
            $table->text('comments')->nullable();
            $table->timestamps();

            $table->foreign('proposal_id')
                ->references('id')->on('proposals')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposal_histories');
    }
}
