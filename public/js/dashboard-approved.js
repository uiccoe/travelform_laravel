$(document).ready(function () {
    $('#nav-tab').on("click", "a", function (event) {
        console.log("INSIDE");
        var activeTab = $(this).attr('href').split('-')[1];
        if (activeTab == 'pending') {
            window.location = "/";
        } else if (activeTab == 'denied') {
            window.location = "/denied";
        } else if (activeTab == 'expired') {
            window.location = "/expired";
        }
    });
});
