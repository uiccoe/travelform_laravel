$(document).ready(function () {
    $('#nav-tab').on("click", "a", function (event) {
        var activeTab = $(this).attr('href').split('-')[1];
        if (activeTab == 'pending') {
            window.location = "/";
        } else if (activeTab == 'approved') {
            window.location = "/approved";
        } else if (activeTab == 'denied') {
            window.location = "/denied";
        }

    });

    $("body").tooltip({ selector: '[data-toggle=tooltip]' });
});
