$(document).ready(function () {
    $('#nav-tab').on("click", "a", function (event) {
        var activeTab = $(this).attr('href').split('-')[1];
        if (activeTab == 'approved') {
            window.location = "/requests/history/approved";
        } else if (activeTab == 'denied') {
            window.location = "/requests/history/denied";
        } else if (activeTab == 'expired') {
            window.location = "/requests/history/expired";
        }
    });
});
