function confirmInput() {
    display_destination.innerHTML = document.getElementById("destination") !== null ? document.getElementById("destination").value : null;
    display_departureDate.innerHTML = document.getElementById("datepicker") !== null ? document.getElementById("datepicker").value : null;
    display_duration.innerHTML = document.getElementById("duration") !== null ? document.getElementById("duration").value : null;
    display_amount.innerHTML = document.getElementById("expense_amount") !== null ? document.getElementById("expense_amount").value : null;
    display_funding.innerHTML = document.getElementById("funding_source") !== null ? document.getElementById("funding_source").value : null;
    display_purpose.innerHTML = document.getElementById("purpose") !== null ? document.getElementById("purpose").value : null;
    display_coverage.innerHTML = document.getElementById("coverage") !== null ? document.getElementById("coverage").value : null;

}

function validateAmount() {
    var input = document.getElementById('expense_amount');
    var decimalPoint = input.value.split('.').length;
    var slash = input.value.split('-').length;
    if (decimalPoint > 2)
        input.value = input.value.substr(0, (input.value.length) - 1);

    if (slash > 2)
        input.value = input.value.substr(0, (input.value.length) - 1);

    input.value = input.value.replace(/[^0-9.-]/, '');

    if (decimalPoint == 2)
        input.value = input.value.substr(0, (input.value.indexOf('.') + 3));

    if (input.value == '.' || input.value == '-')
        input.value = "";
}

function checkDate() {
    var selectedText = document.getElementById('datepicker').value;
    var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    if (date_regex.test(selectedText)) {
        var selectedDate = new Date(selectedText);
        var now = new Date();
        now.setHours(0, 0, 0, 0);
        if (selectedDate < now) {
            error_message.innerHTML = "Departure date can't be in the past";
            document.getElementById('datepicker').value = null;
        } else {
            error_message.innerHTML = ''
        }
    } else {
        error_message.innerHTML = 'Enter a valid date.';
        document.getElementById('datepicker').value = null;
    }

}
