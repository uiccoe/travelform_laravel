$(function(){
    // bind change event to select
    $('#yearFilter').on('change', function () {
        var url = $(this).val(); // get selected value
        if (url) { // require a URL
            window.location = url;
            $('#yearFilter').val($(this).val());// redirect
        }
        return false;
    });
});
