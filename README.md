# Laravel UIC Template

This is a template for PHP Laravel with Shibboleth compatibility for UIC

## Installation

Install composer dependecies

```
composer install
```

- ***You will get an error regarding missing Shibalike files.*** Delete the folder ```vendor/dpazuic``` and replace it with the ```dpazuic``` folder in the zip file [(link)](https://drive.google.com/open?id=1nzmzXdkgVDms5mV7d3RnfPYzFuT9AISi)

Copy the file ```.env.example``` as ```.env``` and fill out the database and shibboleth/shibalike properties

```
...

DB_HOST= ?
DB_DATABASE= ?
DB_USERNAME= ?
DB_PASSWORD= ?

...

SHIBALIKE=true
SHIBBOLETH_CREATE_USER=false
```

Generate an application key

```
php artisan key:generate
```

Migrate the tables

```
php artisan migrate
```

Run the development server

```
php artisan serve
```

## Notes

By default, ***a user must exist in the users table in order to log in.***

- Setting ```SHIBBOLETH_CREATE_USER=true``` in the ```.env``` file will create the user after logging in (if not already present in the database)

    - ***Warning:*** *This essentially grants access to all users who can authenticate*

    - ***Warning:*** *This will not create relevant models and relationships, if any are needed*

You can use the Shibalike emulator in your development environment by setting ```SHIBBALIKE=true``` in the ```.env``` file

- Users for Shibalike emulation are stored in ```config/shibboleth.php```. *The password is the same as the username.*
